import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IUsers } from '../services/interface/IUsers';
import { IHeaders } from '../services/interface/IHeaders';
import { IStates } from '../services/interface/IStates';
import { Observable } from 'rxjs/Observable';
import 'rxjs/operator/map';
@Injectable()
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<IUsers[]> {
    return this.http.get<IUsers[]>('../../../assets/json/users.json');
  }

  getHeaders(): Observable<IHeaders[]> {
    return this.http.get<IHeaders[]>('../../../assets/json/header.json');
  }

  getStates(): Observable<IStates[]> {
    return this.http.get<IStates[]>('../../../assets/json/states.json');
  }
}
