export interface IUsers {
  firstname: string;
  lastname: string;
  email: string;
}
