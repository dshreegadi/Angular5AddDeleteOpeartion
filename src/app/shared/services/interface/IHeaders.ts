export interface IHeaders {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  age: number;
  state: string;
}
