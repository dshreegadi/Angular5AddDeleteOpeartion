import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { UsersService } from '../../shared/services/users.service';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaginatorService } from '../../shared/services/pagination/paginator.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public users;
  public headers;
  public states;
  userForm: FormGroup;

  isAdded: boolean;
  isModal: boolean;
  isDelete: boolean;
  isChecked: boolean;
  isEditable: boolean;
  isFocused: any;
  selectedIDs: any[];
  isSelectAll: any;
  selectedIDCount: any;
  showSaveBtn: boolean;
  isEdit: any[];
  ismymodal: boolean;
  isDrop: boolean;

  // array of all items to be paged
  private allItems: any[];

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];


  emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
  firstnamePattern = '([A-Z][a-zA-Z]*)';
  lastnamePattern = '([A-Z][a-zA-Z]*)';

  constructor(private userService: UsersService,
    private paginatorService: PaginatorService,
    private fb: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef) {
    this.userForm = this.fb.group({
      firstname: new FormControl(null, [Validators.required]),
      lastname: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.pattern(this.emailPattern)]),
    });

    this.isModal = false;
    this.isAdded = false;
    this.isDelete = false;
    this.isChecked = false;
    this.selectedIDs = [];
    this.isSelectAll = false;
    this.selectedIDCount = 0;
    this.users = [];
    this.headers = [];
    this.states = [];
    this.isEditable = false;
    this.showSaveBtn = false;
    this.isEdit = [];
    this.ismymodal = false;
    this.isDrop = false;
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(data => this.users = data);
    this.isSelectAll = false;


    this.userService.getUsers()
      .subscribe(data => {
        this.allItems = data;
        this.setPage(1);
      });

    this.userService.getHeaders()
      .subscribe(headerData => this.headers = headerData);
    this.userService.getStates()
      .subscribe(states => this.states = states);
  }

  onSubmit() {
    if (this.userForm.valid) {
      this.users.push(this.userForm.value);
      document.getElementById('id01').style.display = 'none';
      this.userForm.reset();
    }
  }

  deleteUser() {
    if (this.isChecked === true) {
      for (let i = this.selectedIDs.length - 1; i >= 0; i--) {
        if (this.selectedIDs[i]) {
          this.users.splice(i, 1);
          this.isSelectAll = false;
          this.selectedIDCount = 0;
        }
      }
    }
    this.selectedIDs = [];
    this.isEdit = [];
  }

  showModal() {
    document.getElementById('id01').style.display = 'block';
  }

  closeModal() {
    document.getElementById('id01').style.display = 'none';
  }

  checkAll(event) {
    this.isChecked = true;
    this.isEditable = false;
    if (event) {
      for (let i = 0; i <= this.users.length; i++) {
        this.selectedIDs[i] = true;
      }
      this.selectedIDCount = this.users.length;
      this.isSelectAll = true;
    } else {
      for (let i = 0; i <= this.users.length; i++) {
        this.selectedIDs[i] = false;
        this.selectedIDCount = 0;
      }
    }
  }

  check(event, user, i) {

    if (event.target.checked) {
      this.selectedIDs[i] = true;
      this.selectedIDCount++;
      this.isEditable = false;
      if (this.selectedIDCount === this.users.length) {
        this.isSelectAll = true;
      }
    } else {
      this.selectedIDCount--;
      this.isSelectAll = false;
    }
    this.isChecked = true;
  }
  editData(id, classname) {
    this.isEdit = [];
    if (classname === 'fname') {
      this.isEdit[id + classname] = true;

    }
    if (classname === 'lname') {
      this.isEdit[id + classname] = true;
    }
    if (classname === 'email') {
      this.isEdit[id + classname] = true;
    }
  }

  saveUser(id, classname) {
    this.isEdit = [];
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.paginatorService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
