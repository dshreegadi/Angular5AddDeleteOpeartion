import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { UsersComponent } from './components/users/users.component';
import { UsersService } from './shared/services/users.service';
import { PaginatorService } from './shared/services/pagination/paginator.service';
import { PaginationComponent } from '../app/shared/components/pagination/pagination.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [UsersService, PaginatorService],

  bootstrap: [AppComponent]
})
export class AppModule { }
